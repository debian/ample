#undef TRUE
#define TRUE 1
#undef FALSE
#define FALSE 0

/* NOTE: This is signed since it's used somewhat different than common 
 * in some conditions: 
 * < 0 - undefined
 * > 0 - TRUE
 * = 0 - FALSE
 */
#undef bool
#define bool signed short int

#define min(x,y) ((x) < (y) ? (x) : (y))
#define max(x,y) ((x) > (y) ? (x) : (y))

struct global_config {
	int port;
	bool inetd;
	bool order;
	bool trace;
	bool recursive;
	int debuglevel;
	int max_clients;
	char * username;
	char * password;
	char **pathlist;
	char * program_name;
	char * logfile;
	char * conffile;
	char * htmlfile;
	char * htmlheader;
	char * htmlmiddle;
	char * htmlfooter;
	char * servername;
	char * serveraddress;
	char * filter;
};

struct childstat {
	pid_t childpid;
	char *client;
	char *status;
};

#define MODE_INDEX    0x01
#define MODE_METADATA 0x02
#define MODE_SINGLE   0x04
#define MODE_M3U      0x08
#define MODE_RM3U     0x10
#define MODE_PARTIAL  0x20
#define MODE_BASIC    0x40
#define MODE_INFO     0x80

#define MODE_SET(x) do { cconf->mode |= x; } while(0)
#define MODE_ISSET(x) (cconf->mode & x)

extern struct global_config gconf;
extern struct childstat *childarray;
